#include <vector>
#include<iostream>
using namespace std;

template<class T>
void Sort(T data[], int arrSize)
{
	int start, current;
	int gap = (arrSize * 2) / 3;
	while (gap > 0)
	{
		for (start = gap; start < arrSize; start++)
		{
			// a comment
			current = start;
			while (current >= gap && data[current] < data[current - gap])
			{
				swap(data[current], data[current - gap]);
				current -= gap;
			}
		}
		gap = (gap * 5) / 9;
	}
}

template<class T>
void Sort(vector<T>& V)
{
	int start, current;
	int gap = (V.size() * 2) / 3;
	while (gap > 0)
	{
		for (start = gap; start < V.size(); start++)
		{
			current = start;
			while (current >= gap && V[current] < V[current - gap])
			{
				swap(V[current], V[current - gap]);
				current -= gap;
			}
		}
		gap = (gap * 5) / 9;
	}
}
//template<class T>
//void Sort(vector<T>& V)
//{
//	vector<T>::iterator itr = V.begin();
//	int gap = (V.size() * 2) / 3;
//	while (gap > 0)
//	{
//		for (itr = gap; itr != V.end(); itr++)
//		{
//			while (itr >= gap && *itr < *itr[itr - gap])
//			{
//				swap(*itr, *itr[itr - gap]);
//				itr -= gap;
//			}
//		}
//		gap = (gap * 5) / 9;
//	}
//}


template<class T>
int Lsearch(const T data[], int arrSize, const T& target)
{
	for (int i = 0; i < arrSize; i++)
	{
		if (data[i] == target)
		{
			return i;
		}
	}
	return -1;
}

template<class T>
int Count(vector<T>& V, const T& target)
{
	int count = 0;
	vector<T>::iterator i;
	for (i = V.begin(); i != V.end(); i++)
	{
		if (*i == target)
		{
			count++;
		}
	}
	return count;
}

