/*
Jackie Batson
CS 201
Program 5
Due 7/19/15

Description: Create template functions to sort an array of ints, doubles, and
strings; sort a vector of ints, doubles, or strings; search an array of the
above types for a target; count how many times a target of the above types 
appears in a vector.

Input: none
Output: prints out results of the various template function calls to the screen

Algorithm:
1. Create an array and vector of random length 3-15 and random integers 1-999.
2. Create an array and vector of random length 3-15 and random doubles 0.1-99.9
3. Create an array and vector of random length 3-10 and strings of random
   lengths 2-8 characters.
4. Build templates to sort arrays and vectors using Shell Sort
   a. use initial gap size 2/3
   b. use subsequent gap size 5/9 as shown in class notes
5. Print out each unsorted array and vector, then print out the sorted
   arrays and vectors.
6. Build a template to search the array using a linear search. Return the first
   position that the target is found; return -1 if not found.
7. Build a template to iterate over a vector and return the number of times a
   target appears.
8. First set the search and count targets to values out of range, known not to
   be found, and call the respective functions.
9. Change the targets to values known to be found and repeat the search and
   count.
10. Print all results of sorts, searches, and counts of arrays and integers of
   ints, doubles, and strings to the screen.
*/


#include "TemplateFunctions.h"
#include <vector>
#include <string>
#include <iostream>
using namespace std;

string getRandomString(int length);
//PRE: an integer for the length of the string
//POST: a string of the passed in length is randomly generated from
//the 26 characters of the lowercase alphabet
//RETURNS: a string of randomly generated characters from the lowercase
//alphabet.

double getRandomDouble();
//PRE: nothing
//POST: generates a random double from 0.1-99.9
//RETURNS: a double, the randomly chosen double

void makeIntArray();
//PRE: nothing
//POST: randomly makes an integer array of random length and posts the results 
//of a sort and search of the array to the screen. First the array is printed 
//unsorted, then sorted, then it is searched for a value known to not be found, 
//then a value known to be found, and prints all of the results to the screen.

void makeIntVector();
//PRE: nothing
//POST: randomly makes an integer vector of random length and posts the results 
//of a sort and count of the vector to the screen. First the vector is printed 
//unsorted, then sorted, then it is searched for a value known to not be found, 
//then a value known to be found, and prints all of the results to the screen.

void printIntArrayResults(int p[], int arrSize);
//PRE: a pointer to an array of integers, and the size of the array
//POST: posts the results of a sort and search of the array to the screen.
//First the array is printed unsorted, then sorted, then it is searched for a  
//value known to not be found, then a value known to be found, and prints all of 
//the results to the screen.

void printIntVectorResults(vector<int>& v);
//PRE: a vector of integers
//POST: posts the results of a sort and count of the vector to the screen. First 
//the vector is printed unsorted, then sorted, then it is searched for a value 
//known to not be found, then a value known to be found, and prints all of the 
//results to the screen.

void makeDoubleArray();
//PRE: nothing
//POST: randomly makes a double array of random length and posts the results 
//of a sort and search of the array to the screen. First the array is printed 
//unsorted, then sorted, then it is searched for a value known to not be found, 
//then a value known to be found, and prints all of the results to the screen.

void makeDoubleVector();
//PRE: nothing
//POST: randomly makes a double vector of random length and posts the results 
//of a sort and count of the vector to the screen. First the vector is printed 
//unsorted, then sorted, then it is searched for a value known to not be found, 
//then a value known to be found, and prints all of the results to the screen.

void printDoubleArrayResults(double d[], int arrSize);
//PRE: a pointer to an array of doubles, and the size of the array
//POST: posts the results of a sort and search of the array to the screen.
//First the array is printed unsorted, then sorted, then it is searched for a  
//value known to not be found, then a value known to be found, and prints all of 
//the results to the screen.

void printDoubleVectorResults(vector<double>& v);
//PRE: a vector of doubles
//POST: posts the results of a sort and count of the vector to the screen. First 
//the vector is printed unsorted, then sorted, then it is searched for a value 
//known to not be found, then a value known to be found, and prints all of the 
//results to the screen.

void makeStringArray();
//PRE: nothing
//POST: randomly makes a string array of random length, and strings of random
//lengths, and posts the results of a sort and search of the array to the screen. 
//First the array is printed unsorted, then sorted, then it is searched for a 
//value known to not be found, then a value known to be found, and prints all
// of the results to the screen.

void makeStringVector();
//PRE: nothing
//POST: randomly makes a string vector of random length and random string  
//lengths and posts the resultsof a sort and count of the vector to the screen. 
//First the vector is printed unsorted, then sorted, then it is searched for a  
//value known to not be found, then a value known to be found, and prints all of 
//the results to the screen.

void printStringArrayResults(string s[], int arrSize);
//PRE: a pointer to an array of strings, and the size of the array
//POST: posts the results of a sort and search of the array to the screen.
//First the array is printed unsorted, then sorted, then it is searched for a  
//value known to not be found, then a value known to be found, and prints all of 
//the results to the screen.

void printStringVectorResults(vector<string>& v);
//PRE: a vector of strings
//POST: posts the results of a sort and count of the vector to the screen. First 
//the vector is printed unsorted, then sorted, then it is searched for a value 
//known to not be found, then a value known to be found, and prints all of the 
//results to the screen.

void printPosition(int index);
//PRE: an integer for an index that is the result of a search on an array or
//vector
//POST: "not found" is printed if the index is -1, "found at position" index is
//printed if index is not -1.


int main()
{
	//Integers
	//Calls and prints results of sort and search of integer array 
	makeIntArray();
	//Calls and prints results of sort and count of integer vector
	makeIntVector();

	//Doubles
	//Calls and prints results of sort and search of double array 
	makeDoubleArray();
	//Calls and prints results of sort and count of double vector
	makeDoubleVector();

	//Strings
	//Calls and prints results of sort and search of string array
	makeStringArray();
	//Calls and prints results of sort and count of string vector
	makeStringVector();
	
	system("pause");
	return 0;
}

string getRandomString(int length)
{
	string s;
	s.resize(length);
	//strings generated from lowercase alphabet only
	string alphabet = "abcdefghijklmnopqrstuvwxyz";
	for (int i = 0; i < length; i++)
	{
		int idx = rand() % 25;
		s[i] = alphabet[idx];
	}
	return s;
}

double getRandomDouble()
{
	double x;
	int a, b;
	a = rand() % 100; //gets random int from 0-99 
	b = rand() % 9 + 1; //gets random int from 1-9
	//moves the decimal point in b to make a decimal and adds it to a to
	//generate a double
	x = a + b / 10.0;
	return x;
}
void makeIntArray()
{
	int intArrLength = rand() % 12 + 3; //array length random from 3-15
	int *p = new int[intArrLength];
	for (int i = 0; i < intArrLength; i++)
	{
		p[i] = rand() % 999 + 1; //random integers from 1-999
		
	}
	printIntArrayResults(p, intArrLength); //results printed to screen
	delete[] p;
}
void makeIntVector()
{
	vector<int> IntVec;
	int vecLength = rand() % 12 + 3; //array length random from 3-15
	for (int i = 0; i < vecLength; i++)
	{
		int value = rand() % 999 + 1; //random integers from 1-999
		IntVec.push_back(value);
	}
	printIntVectorResults(IntVec); //results printed to screen
}

void printIntArrayResults(int p[], int arrSize)
{
	cout << "Unsorted int array values: ";
	printArray(p, arrSize);
	Sort(p, arrSize);
	cout << endl << "Sorted int array values: ";
	printArray(p, arrSize);
	int target = 0;                                //known not to exist
	int idx = Lsearch(p, arrSize, target);
	cout << endl << "Search for 0: ";
	printPosition(idx);
	target = p[0];                                 //known to exist
	idx = Lsearch(p, arrSize, target);
	cout << endl << "Search for " << target << ": ";
	printPosition(idx);
	cout << endl;
}
void printIntVectorResults(vector<int>& v)
{
	cout << "Unsorted int vector values: ";
	printVector(v);
	Sort(v);
	cout << endl << "Sorted int vector values: ";
	printVector(v);
	int target = 0;                                  //known not to exist
	int count = Count(v, target);
	cout << endl << "Count for 0: " << count << endl;
	target = v[0];                                   //known to exist
	count = Count(v, target);
	cout << endl << "Count for " << target << ": " << count << endl << endl;
}
void makeDoubleArray()
{
	int dArrLength = rand() % 12 + 3; //array length is random from 3-15
	double *d = new double[dArrLength];
	for (int i = 0; i < dArrLength; i++)
	{
		d[i] = getRandomDouble();
	}
	printDoubleArrayResults(d, dArrLength); //prints results to screen
	delete[] d;
}

void printDoubleArrayResults(double d[], int arrSize)
{
	cout << "Unsorted double array values: ";
	printArray(d, arrSize);
	Sort(d, arrSize);
	cout << endl << "Sorted double array values: ";
	printArray(d, arrSize);
	double target = 0.0;                                //known not to exist
	int idx = Lsearch(d, arrSize, target);
	cout << endl << "Search for 0.0: ";
	printPosition(idx);
	target = d[0];                                      //known to exist
	idx = Lsearch(d, arrSize, target);
	cout << endl << "Search for " << target << ": ";
	printPosition(idx);
	cout << endl;
}
void makeDoubleVector()
{
	vector<double> doubleVec;
	int vecLength = rand() % 12 + 3; //vector length random from 3-15
	for (int i = 0; i < vecLength; i++)
	{
		double value = getRandomDouble();
		doubleVec.push_back(value);
	}
	printDoubleVectorResults(doubleVec); //prints results to screen
}

void printDoubleVectorResults(vector<double>& v)
{
	cout << "Unsorted double vector values: ";
	printVector(v);
	Sort(v);
	cout << endl << "Sorted double vector values: ";
	printVector(v);
	double target = 0.0;                                   //known not to exist
	int count = Count(v, target);                          
	cout << endl << "Count for 0.0: " << count << endl;
	target = v[0];                                         //known to exist
	count = Count(v, target);
	cout << endl << "Count for " << target << ": " << count << endl << endl;
}

void makeStringArray()
{
	int strArrLength = rand() % 7 + 3; //array length random from 3-10.
	string *s = new string[strArrLength];
	for (int i = 0; i < strArrLength; i++)
	{
		int strLength = rand() % 6 + 2; //string length is random from 2-8 characters
		s[i] = getRandomString(strLength);
	}
	printStringArrayResults(s, strArrLength);
	delete[] s;
}
void makeStringVector()
{
	vector<string> stringVec;
	int vecLength = rand() % 7 + 3; //vector length random from 3-10.
	for (int i = 0; i < vecLength; i++)
	{
		int strLength = rand() % 6 + 2; //string length random from 2-8 characters
		stringVec.push_back(getRandomString(strLength));
	}
	printStringVectorResults(stringVec);
}

void printStringArrayResults(string s[], int arrSize)
{
	cout << "Unsorted string array values: ";
	printArray(s, arrSize);
	Sort(s, arrSize);
	cout << endl << "Sorted string array values: ";
	printArray(s, arrSize);
	string target = "jackie";                           //probably not found
	int idx = Lsearch(s, arrSize, target);
	cout << endl << "Search for \"jackie\": ";
	printPosition(idx);
	target = s[0];                                      //definitely should be found
	idx = Lsearch(s, arrSize, target);
	cout << endl << "Search for \"" << target << "\": ";
	printPosition(idx);
	cout << endl;
}
void printStringVectorResults(vector<string>& v)
{
	cout << "Unsorted string vector values: ";
	printVector(v);
	Sort(v);
	cout << endl << "Sorted string vector values: ";
	printVector(v);
	string target = "jackie";                             //probably not found
	int count = Count(v, target);
	cout << endl << "Count for \"jackie\": " << count << endl;
	target = v[0];                                        //definitely should be found
	count = Count(v, target);
	cout << endl << "Count for " << target << ": " << count << endl << endl;
}

void printPosition(int index)
{
	if (index == -1)
		cout << "not found" << endl;
	else
		cout << "found at position " << index << endl;
}