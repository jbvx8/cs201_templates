#pragma once
#include <vector>
#include <iostream>
using namespace std;

template<class T>
//PRE: passes in an array of any type and an integer for the size
//POST: uses Shell's sort algorithm to sort the array in place
void Sort(T data[], int arrSize)
{
	int start, current;
	int gap = (arrSize * 2) / 3; //begin with a gap of 2/3 the size
	while (gap > 0)
	{
		for (start = gap; start < arrSize; start++)
		{
			current = start;
			while (current >= gap && data[current] < data[current - gap])
			{
				swap(data[current], data[current - gap]);
				current -= gap;
			}
		}
		gap = (gap * 5) / 9; //subsequent gaps are 5/9 of the gap
	}
}

template<class T>
//PRE: passes in a vector of any length and data type
//POST: uses Shell's sort to sort the vector in place
void Sort(vector<T>& V)
{
	int start, current;
	int gap = (V.size() * 2) / 3; //begin with a gap of 2/3 the size
	while (gap > 0)
	{
		for (start = gap; start < V.size(); start++)
		{
			current = start;
			while (current >= gap && V[current] < V[current - gap])
			{
				swap(V[current], V[current - gap]);
				current -= gap;
			}
		}
		gap = (gap * 5) / 9; //subsequent gaps are 5/9 of the gap
	}
}

template<class T>
//PRE: passes in an array of any type, the size of the array, and a target
//of any type. 
//POST: Performs a basic linear search for the target
//RETURNS: the index position of the first occurence of the target.
int Lsearch(const T data[], int arrSize, const T& target)
{
	for (int i = 0; i < arrSize; i++)
	{
		if (data[i] == target)
		{
			return i;
		}
	}
	return -1;
}

template<class T>
//PRE: a vector of any data type and a target of any data type
//POST: Performs a basic linear search for the target
//RETURNS: the number of times the target is found in the vector
int Count(vector<T>& V, const T& target)
{
	int count = 0;
	vector<T>::iterator i;
	for (i = V.begin(); i != V.end(); i++)
	{
		if (*i == target)
		{
			count++;
		}
	}
	return count;
}

template<class T>
//PRE: an array of any type and the size of the array
//POST: values in the array are printed to the screen on a single line,
//separated by spaces.
void printArray(T data[], int arrSize)
{
	for (int i = 0; i < arrSize; i++)
	{
		cout << data[i] << " ";
	}
	cout << endl;
}

template<class T>
//PRE: a vector of any type
//POST: values in the vector are printed to the screen on a single line,
//separated by spaces.
void printVector(vector<T>& V) 
{
	vector<T>::iterator i;
	for (i = V.begin(); i != V.end(); i++)
	{
		cout << *i << " ";
	}
	cout << endl;
}


